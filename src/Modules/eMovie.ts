export default interface MovieState {
    "id": string,
    "name": string,
    "subName": string,
    "imagePortrait": string,
}