import React from 'react';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import "./common.css";
import "./App.scss"
import "./Library/slick-carousel/Slick-Convert.scss"
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
// import Login from "./components/Login/Login";
// import Register from "./components/Register/Register";
import Footer from "./components/Footer/Footer";
import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
    return (
      <div className="App">
            <BrowserRouter>
                <Header />
                <Routes>
                    <Route path="/" element={<Home />} />
                    {/* <Route path="/Login" element={<Login />} /> */}
                    {/* <Route path="/Register" element={<Register />} /> */}
                </Routes>
                <Footer />
                <ToastContainer /> 
            </BrowserRouter>
        </div>
    );
}

export default App;