import React from 'react';
import "./MovieThisWeek.scss";
import Slider from "react-slick"
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {settings} from "../MovieNow/MovieNow";
import getWeekNumberMovie from "../../Logic/getWeekNumberMovie";


function MovieThisWeek(props:any) {

    return (
        <Slider className="box-movie" {...settings}>
            {
                props.data && props.data.movieShowing.filter((n:any)=>{return getWeekNumberMovie(n.startdate)})
                    .map((n: any) => {
                        return <div className="movie fl fl-col">
                            <div className="img-cover">
                                <img src={n.imagePortrait}
                                     alt="avatar"/>
                            </div>
                            <p>{n.name}</p>
                            <p className="subname">{n.subName}</p>
                        </div>
                    }
                )
            }
        </Slider>
    );
}

export default MovieThisWeek;