import React, {useEffect, useState} from 'react';
import "./Home.scss";
import MovieNow from "./MovieNow/MovieNow";
import MovieThisWeek from "./MovieThisWeek/MovieThisWeek";
import {useAppSelector, useAppDispatch} from "../../Redux/Hooks";
import {GetMovie} from "../../Redux/Reducer/rdcMovie";
import MovieSoon from "./MovieSoon/MovieSoon";

function Home() {
    const [film, setFim] = useState();
    useEffect(() => {
        fetch("https://teachingserver.onrender.com/cinema/nowAndSoon")
            .then(res => res.json())
            .then(data => {
                setFim(data)
            })
    }, [])
    return (
        <div>
            <div className="now-movie fl fl-cen">
                <div className="mainSize">
                    <div className="title-bar fl fl-spw mb-20">
                        <h3>PHIM ĐANG CHIẾU</h3>
                        <a>SEE ALL</a>
                    </div>
                    <MovieNow data={film}/>
                </div>
            </div>

            <div className="this-week-movie fl fl-cen">
                <div className="mainSize">
                    <div className="title-bar fl fl-spw mb-20">
                        <h3>PHIM CHIẾU TRONG TUẦN</h3>
                        <a>SEE ALL</a>
                    </div>
                    <MovieThisWeek data={film}/>
                </div>
            </div>

            <div className="soon-movie fl fl-cen">
                <div className="mainSize">
                    <div className="title-bar fl fl-spw mb-20">
                        <h3>PHIM SẮP CHIẾU</h3>
                        <a>SEE ALL</a>
                    </div>
                    <MovieSoon data={film}/>
                </div>
            </div>

        </div>
    );
}

export default Home;