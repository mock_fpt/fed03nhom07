import React from 'react';
import "./MovieNow.scss";
import Slider from "react-slick"
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import getWeekNumberMovie from "../../Logic/getWeekNumberMovie";

export const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 6,
    slidesToScroll: 2,
    initialSlide: 0,
    arrows: true,
    autoplaySpeed: 5000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                initialSlide: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};

function MovieNow(props: any) {
    return (
        <Slider className="box-movie" {...settings}>
            {
                props.data && props.data.movieShowing.filter((n:any)=>{return !getWeekNumberMovie(n.startdate)})
                    .map((n: any) => {
                        return <div className="movie fl fl-col">
                            <div className="img-cover">
                                <img src={n.imagePortrait}
                                     alt="avatar"/>
                            </div>
                            <p>{n.name}</p>
                            <p className="subname">{n.subName}</p>
                        </div>
                    }
                )
            }
        </Slider>

    );
}

export default MovieNow;