import React from 'react';
import "./Header.scss"
import {BsSearch} from 'react-icons/bs'
import { FaUserLock } from "react-icons/fa";
import {Link, useParams} from "react-router-dom";

function Header() {
    return (
        <div className="header fl fl-cen pt-10 pb-10">
            <div className="mainSize fl fl-mid fl-spw">
                <div className="fl fl-mid">
                    <Link to={"/"}>
                        <img src={require("../../Img/logo_Desktop.png")} alt="Logo"/>
                    </Link>
                    <div className="Search relative ml-10">
                        <form className="Search-Form fl fl-cen fl-mid">
                            <BsSearch className="icon-search absolute ml-10"/>
                            <input type="text" className="rad-20 pl-40 pt-10 pb-10 w-100"
                                   placeholder="Movie or theater"/>
                        </form>
                    </div>
                </div>
                <div className="fl fl-mid">
                    <ul className="fl bx-siz">
                        <li><a href="/">Rạp</a></li>
                        <li><a href="/">Phim</a></li>
                        <li><a href="/">Tin tức</a></li>
                    </ul>
                    <Link to={"/Login"}>
                    <FaUserLock className="icon-login"/>
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default Header;