import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {Provider} from "react-redux";
import rdcMovie from "./Redux/Reducer/rdcMovie";
import {configureStore} from "@reduxjs/toolkit";

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);

const store = configureStore({
    reducer: {
        Movie: rdcMovie
    }
})

root.render(
        <Provider store={store}>
            <App/>
        </Provider>
);

