import rdcMovie from "./Reducer/rdcMovie";
import {combineReducers, configureStore} from "@reduxjs/toolkit";

const store = configureStore({
    reducer: {
        Movie: rdcMovie
    }
})

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export default store