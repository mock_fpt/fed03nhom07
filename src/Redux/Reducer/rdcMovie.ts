import {createSlice} from "@reduxjs/toolkit";
import type {RootState} from "../Store";
import MovieState from "../../Modules/eMovie";

interface State {
    lsMovie: MovieState[]
}

const initialState: State = {
    lsMovie: [] || null
}

export const rdcMovie = createSlice({
    name: "Movie",
    initialState,
    reducers: {
        GetMovie: (state: State = initialState, action:any) => {
            state.lsMovie = action.payload;
        }
    }

});

export const selectMovie = (state: RootState) => state.Movie;

export const {GetMovie} = rdcMovie.actions;

export default rdcMovie.reducer;

